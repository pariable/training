const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Express Api!')
});

const bodyParser = require('body-parser');

//allowed reuest data format
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const postRouter = require('./routes/posts');
app.use('/api/posts',postRouter);

const userRouter = require('./routes/users')
app.use('/api/users',userRouter)

app.listen(port, () => {
  console.log(`app running at http://localhost:${port}`)
})