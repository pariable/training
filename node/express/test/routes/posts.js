const express = require('express');
const router = express.Router();
const connection = require('../config/database');
const {body,validationResult} = require('express-validator');

//index
router.get('/',function(req,res){
    connection.query('select * from posts order by id desc',function(err,rows){
        if(err){
            return res.status(500).json({
                status:false,
                message:"Internal Server Error!"
            })
        }else{
            return res.status(200).json({
                status:true,
                message:"List data posts",
                data:rows
            })
        }
    })
});

//store post
router.post('/save',[
    body('IdUser').notEmpty(),
    body('title').notEmpty(),
    body('content').notEmpty()
],(req,res)=>{
    const error = validationResult(req);
    if(!error.isEmpty()){
        return res.status(422).json({
            errors:error.array()
        })
    }

    let formData={
        IdUser:req.body.IdUser,
        title : req.body.title,
        content:req.body.content
    }

    connection.query('insert into posts set ? ',formData,function(err,rows){
        if(err){
            return res.status(500).json({
                status:false,
                message:"Internal Server"
            })
        }else{
            return res.status(201).json({
                status:true,
                message:'Insert Ok',
                data:rows[0]
            })
        }
    })

})

module.exports=router;