const express = require('express')
const router = express.Router()
const con = require('../config/database')
const {body,validationResult} = require('express-validator')

router.get('/',function(req,res){
    con.query("SELECT * FROM users ORDER BY id",function(err,rows){
        if(err){
            return res.status(500).json({
                status:false,
                message:"Internal server error"
            })
        }else{
            return res.status(200).json({
                status:true,
                message:"Data Ok",
                data:rows

            })
        }
    })
})


//post
router.post('/save',[
    body('FirstName').notEmpty(),
    body('UserName').notEmpty(),
    body('Password').notEmpty()
],(req,res)=>{
    const error = validationResult(req)
    if(!error.isEmpty()){
        return res.status(422).json({
            errors:error.array()
        })
    }

    let formData={
        FirstName : req.body.FirstName,
        UserName    : req.body.UserName,
        Password    :   req.body.Password
    }

    con.query("INSERT INTO users SET ? ",formData,function(err,rows){
        if(err){
            return res.status(500).json({
                status: false,
                message:    "Internal Server"
            })
        }else{
            return res.status(201).json({
                result:true,
                message:"Insert Ok",
                data:rows[0]
            })
        }
    })

})

module.exports=router

