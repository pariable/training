﻿using Microsoft.VisualBasic;
using System;

/**
 * @author edwin warming gunawan
 * @email  kubilk56@gmail.com
 * 09-12-2023
 */


public class Program
{
    static String sort(String str)
    {
        String s = "";
        int c = str.Length;

        for (int j = 0; j < str.Length; j++)
        {
            if (c > 0)
            {
                for (int k = (str.Length - 1); k >= j; k--)
                {
                    if (str[j] == str[k])
                    {
                        s += str[k];
                        c--;
                    }

                }

            }
            

        }

        return s;

    }
    public static string procVowel(string input)
    {
        //your code here
        string strVowel = "";
        int totalVowels = 0;
        var vowels = new HashSet<char> { 'a', 'e', 'i', 'o', 'u' };

        for (int i = 0; i < input.Length; i++)
        {

            if (vowels.Contains(Char.ToLower(input[i])))
            {
                totalVowels++;
                strVowel += input[i];
            }


        }


        return sort(strVowel.ToLower());
    }
    public static string procConsonant(string input)
    {
        string strConsonant = "";
        int totalConsonants = 0;
        var consonants = new HashSet<char> { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };


        for (int i = 0; i < input.Length; i++)
        {

            if (consonants.Contains(Char.ToLower(input[i])))
            {
                totalConsonants++;
                strConsonant += input[i];
            }

        }

        return sort(strConsonant.ToLower());
    }
    public static void Main()
    {
        while (true)
        {
            Console.Write("Input one line of words (S):");

            string input = Console.ReadLine();

            string charVowel = procVowel(input);
            string charConsonant = procConsonant(input);


            if (string.IsNullOrWhiteSpace(input))
            {
                Console.Write("OK. You pressed Enter. See you agian!");
                break;

            }

            Console.WriteLine("Vowel Characters: ");
            Console.WriteLine(charVowel);

            Console.WriteLine("Consonant Characters: ");
            Console.WriteLine(charConsonant);

            Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");

        }
    }
}
