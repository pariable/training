﻿/**
 * @author edwin warming gunawan
 *
 * 09-12-2023
 */

int NoF;
string MoF;

while (true)
{
    Console.Write("Input the Number of Families:");

    try
    {
        NoF = Convert.ToInt32(Console.ReadLine());
        if (NoF is int)
        {
            Console.Write("Input the Number of members in the Family:");

            MoF = Console.ReadLine();
            MoF = MoF.Replace(" ", "").Trim();
            
            if (string.IsNullOrWhiteSpace(MoF))
            {
                Console.Write("OK. You pressed Enter. See you agian!");
                break;
            }

            int x = MoF.Length;
            float y=0F;
            
            if (x != NoF)
            {
                Console.Write("Input must be equal with count of family!");
            }
            else
            {
                int df = 0;

                for (int i = 0; i < x; i++)
                {
                    
                    df += (MoF[i] - '0');
                    
                }

                y = df / 4;
                if((df % 4) != 0)
                {
                    y ++;
                }
                
                Console.Write("Minimum Bus required is : {0}", y);
            }

        }
        else
        {
            Console.Write("OK. You pressed Not Integer. See you agian!");
            break;
        }

        if (NoF == null)
        {
            Console.Write("OK. You pressed Enter. See you agian!");
            break;
        }


    }
    catch (Exception e)
    {
        Console.Write("OK. You pressed Enter. See you agian!");
        break;

    }
    
    Console.WriteLine("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
}

