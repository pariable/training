﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using BasicMath1;

namespace BasicMathTest
{
    [TestClass]
    public class BasicMathTests
    {
        BasicMaths be = new BasicMaths();
        double res;

        [TestMethod]
        public void TestAdd()
        {
            res = be.Add(10, 10);
            Assert.AreEqual(res,20);

        }

        [TestMethod]
        public void TestSubtract()
        {
            res = be.Subtract(10, 10); 
            Assert.AreEqual(res,0);
        }

        [TestMethod]
        public void TestMultiply()
        {
            res = be.Multiply(10, 10);
            Assert.AreEqual(res,100);

        }

        [TestMethod]
        public void TestDivide()
        {
            res= be.Divide(10, 10);
            Assert.AreEqual(res,1);

        }
    }
}
